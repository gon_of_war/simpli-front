# Front

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.3.6.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Name
API

## Description
Application that allows you to list companies, create new companies, list employees (providing the name of the company to which they belong) and create new employees associated with a particular company.

## Installation

1) git clone https://gitlab.com/gon_of_war/simpli-front.git

2) cd simpli-front

3) npm install (you must have installed npm)

4) ns serve


## Usage

By default, the application consumes an api, which must also be installed and executed locally on port 8000.

- http://localhost:4200/companies => list of companies (GET)

- http://localhost:4200/companies => create a company (POST)

- http://localhost:4200/employees=> list of employes by company (GET)

- http://localhost:4200/employees => create a employee for a particular company (POST)

