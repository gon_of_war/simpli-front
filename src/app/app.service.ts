import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Observable } from 'rxjs'
import { AppComponent } from 'src/app/app.component'

@Injectable({
   providedIn: 'root'
})

export class AppService {
   constructor(
      private _http: HttpClient
   ){ }

   getCompanies(): Observable<any>{
      return this._http.get('http://localhost:8000/api/company/')
   }

   saveCompany(object: any): Observable<any>{
      let params = object;
      return this._http.post('http://localhost:8000/api/company/', params)
   }

   getEmployees(id: number): Observable<any>{
      let params = id;
      return this._http.get(`http://localhost:8000/api/employee/?company=${ params }`)
   }

   saveEmployee(object: any): Observable<any>{
      let params = object;
      return this._http.post('http://localhost:8000/api/employee/', params)
   }
}