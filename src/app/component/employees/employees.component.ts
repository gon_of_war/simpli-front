import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, ReactiveFormsModule, Validators } from '@angular/forms';
import { AppService } from 'src/app/app.service';


@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.css']
})
export class EmployeesComponent implements OnInit {
  employees:any[] = [];
  companies:any[] = [];
  creatingEmployee:boolean = false;
  closeCreating:boolean = true;
  permitCreate: boolean = false;
  company_id: number = 0;

  employeeForm = this.fb.group({
    fullname: new FormControl('', [Validators.required, Validators.maxLength(200), Validators.minLength(6)]),
    rutEmployee: new FormControl('', [Validators.required, Validators.pattern('^[0-9]{7,8}[kK0-9]$')]),
    email: new FormControl('', [Validators.required, Validators.email]),
  })

  constructor(
    private _AppService: AppService,
    private fb: FormBuilder,
  ) { }

  get f(): any {
    return this.employeeForm.controls;
  }

  ngOnInit(): void {
    this.getCompanies();
    this.permitCreate = false;
  }

  getCompanies(){

    this._AppService.getCompanies()
      .subscribe({
        next: (response: any) => {
          if(response != null){
            this.companies = response;
            console.log(this.companies);
            console.log('Getting Companies')
          } else {
            this.companies = [];
          }
        },
        error: (e) => {
          console.log(e);
        },
        complete: () => {
          console.log('OK')
        }
      })
  }

  getEmployees(id: number, back?: boolean){
    this.company_id = id;
    back ? null : this.permitCreate = true;
    // this.creatingCompany = false;
    // this.creatingEmployee = true;
    this._AppService.getEmployees(id)
      .subscribe({
        next: (response: any) => {
          if(response != null){
            this.employees = response;
            console.log(this.employees);
            console.log(this.company_id);
            console.log('Getting Companies')
          } else {
            this.employees = [];
          }
        },
        error: (e) => {
          console.log(e);
        },
        complete: () => {
          console.log('OK')
        }
      })
  }

  getId(company: any){
    console.log(company);
  }

  createEmployee(){
    this.creatingEmployee = true;
    console.log('creating ...')
  }

  closeCreatingE(back?: boolean){
    this.creatingEmployee = false;
    this.permitCreate = false;
    console.log('closing ...');
    this.getEmployees(0, back);
    this.employeeForm.reset();
  }

  saveEmployee(){
    let object = {
      fullname: this.employeeForm.value.fullname,
      rutEmployee: this.employeeForm.value.rutEmployee,
      email: this.employeeForm.value.email,
      company_id: this.company_id
    }

    this._AppService.saveEmployee(object)
    .subscribe({
      next: () => { },
      error: (e) => {
        console.log(e);
      },
      complete: () => {
        this.employeeForm.reset();
        this.getCompanies();
        console.log('OK')
        this.creatingEmployee = false;
      }
    })
  }

}
