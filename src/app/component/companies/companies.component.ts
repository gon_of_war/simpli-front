import { Component, OnInit } from '@angular/core';
import { AppService } from 'src/app/app.service';
import { FormBuilder, FormControl, ReactiveFormsModule, Validators } from '@angular/forms';

@Component({
  selector: 'app-companies',
  templateUrl: './companies.component.html',
  styleUrls: ['./companies.component.css']
})
export class CompaniesComponent implements OnInit {

  companies:any[] = [];
  creatingCompany:boolean = false;
  closeCreating:boolean = true;

  companyForm = this.fb.group({
    name: new FormControl('', [Validators.required, Validators.maxLength(200), Validators.minLength(6)]),
    rut: new FormControl('', [Validators.required, Validators.pattern('^[0-9]{7,8}[kK0-9]$')]),
    address: new FormControl('', [Validators.required, Validators.maxLength(200), Validators.minLength(6)]),
    telephone: new FormControl('', [Validators.required, Validators.maxLength(9), Validators.pattern('^[0-9]{9}$')]),
  })

  constructor(
    private _AppService: AppService,
    private fb: FormBuilder
  ) { }

  get f(): any {
    return this.companyForm.controls;
  }

  ngOnInit(): void {
    this.getCompanies();
  }

  getCompanies(){

    this._AppService.getCompanies()
      .subscribe({
        next: (response: any) => {
          if(response != null){
            this.companies = response;
            console.log(this.companies);
            console.log('Getting Companies')
          } else {
            this.companies = [];
          }
        },
        error: (e) => {
          console.log(e);
        },
        complete: () => {
          console.log('OK')
        }
      })
  }

  createCompany(){
    this.creatingCompany = true;
    console.log('creating ...')
  }

  closeCompany(){
    this.creatingCompany = false;
    console.log('closing ...');
    this.companyForm.reset();
  }

  saveCompany(){
    let object = {
      name: this.companyForm.value.name,
      rut: this.companyForm.value.rut,
      address: this.companyForm.value.address,
      phone: this.companyForm.value.telephone,
    }

    this._AppService.saveCompany(object)
    .subscribe({
      next: () => { },
      error: (e) => {
        console.log(e);
      },
      complete: () => {
        this.getCompanies();
        console.log('OK')
        this.companyForm.reset();
        this.creatingCompany = false;
      }
    })
  }

}
